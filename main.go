package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func main() {
	fmt.Printf("endpoint,nanoseconds\n")

	// Initial setup
	_, isSSL := os.LookupEnv("SSL")
	opts := minio.Options{
		Creds:  credentials.NewStaticV4(os.Getenv("AWS_ACCESS_KEY_ID"), os.Getenv("AWS_SECRET_ACCESS_KEY"), ""),
		Secure: isSSL,
	}

	obj_size := 4096
	if obj_size_str, ok := os.LookupEnv("OBJ_SIZE"); ok {
		obj_size_, err := strconv.Atoi(obj_size_str)
		if err != nil {
			log.Fatal("Failed to parse OBJ_SIZE: ", err)
			return
		} else {
			obj_size = obj_size_
		}
	}

	if region, ok := os.LookupEnv("REGION"); ok {
		opts.Region = region
	}

	if _, ok := os.LookupEnv("SSL_INSECURE"); ok {
		opts.Transport = &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	}

	mc, err := minio.New(os.Getenv("ENDPOINT"), &opts)

	if err != nil {
		log.Fatal("failed connect", err)
		return
	}

	// Create Bucket
	buck := uuid.New().String()
	err = mc.MakeBucket(context.Background(), buck, minio.MakeBucketOptions{})
	if err != nil {
		log.Fatal(err)
		return
	}

	buffer := make([]byte, obj_size)
	// PutObject
	for i := 0; i < 100; i++ {
		// Fill buffer with random bytes
		rand.Read(buffer)
		istr := strconv.Itoa(i)
		start := time.Now()
		_, err := mc.PutObject(context.Background(), buck, "element"+istr, bytes.NewReader(buffer), int64(len(buffer)), minio.PutObjectOptions{ContentType: "application/octet-stream"})
		elapsed := time.Since(start)
		if err != nil {
			log.Fatal("failed putObject: ", err)
			return
		}
		fmt.Printf("putobject,%v\n", elapsed.Nanoseconds())
	}

	// ListObject
	for i := 0; i < 100; i++ {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		start := time.Now()
		objectCh := mc.ListObjects(ctx, buck, minio.ListObjectsOptions{
			Recursive: true,
		})
		for object := range objectCh {
			if object.Err != nil {
				log.Fatal(object.Err)
				return
			}
		}
		elapsed := time.Since(start)
		fmt.Printf("listobjects,%v\n", elapsed.Nanoseconds())
	}

	// GetObject
	for i := 0; i < 100; i++ {
		istr := strconv.Itoa(i)
		start := time.Now()
		object, err := mc.GetObject(context.Background(), buck, "element"+istr, minio.GetObjectOptions{})
		if err != nil {
			log.Fatal(err)
			return
		}
		if _, err = io.Copy(ioutil.Discard, object); err != nil {
			log.Fatal("failed getobject: ", err)
			return
		}
		elapsed := time.Since(start)
		fmt.Printf("getobject,%v\n", elapsed.Nanoseconds())
	}

	// RemoveObject
	for i := 0; i < 100; i++ {
		istr := strconv.Itoa(i)
		start := time.Now()
		err = mc.RemoveObject(context.Background(), buck, "element"+istr, minio.RemoveObjectOptions{})
		elapsed := time.Since(start)
		if err != nil {
			log.Fatal(err)
			return
		}
		fmt.Printf("removeobject,%v\n", elapsed.Nanoseconds())
	}

}
